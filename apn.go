package main

import (
	"os"
	"strconv"

	"github.com/tnako/appnexus"
)

var apnc *appnexus.Client

func apnInit() {
	var err error
	apnc, err = appnexus.NewClient(globalConfig.Appnexus.URI)
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(1)
	}

	loggerSugar.Debugf("APN Auth on '%s'", globalConfig.Appnexus.URI)
	err = apnc.Login(globalConfig.Appnexus.User, globalConfig.Appnexus.Password)
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(1)
	}
}

func apnSetPublisher(pub dbPublisher) (int64, int64) {
	newPub := appnexus.Publisher{Code: strconv.FormatInt(pub.PublisherID, 10), State: pub.State, Name: pub.Name, IsOO: false, ResellingExposure: "public", InventoryRelationship: "indirect_multiple_publishers"}

	if pub.PublisherAppnexusID.Valid {
		newPub.ID = pub.PublisherAppnexusID.Int64
		resp, err := apnc.Publishers.Update(newPub)
		if err != nil {
			loggerSugar.Errorw(err.Error(), "Publisher", newPub)
			os.Exit(3)
		}
		loggerSugar.Debugf("Update apn_publisher_id='%d', name='%s'", resp.Obj.Publisher.ID, resp.Obj.Publisher.Name)
		return resp.Obj.Publisher.ID, resp.Obj.Publisher.BasePaymentRuleID
	}

	resp, err := apnc.Publishers.Add(&newPub)
	if err != nil {
		loggerSugar.Errorw(err.Error(), "Publisher", newPub)
		os.Exit(3)
	}
	loggerSugar.Debugf("New apn_publisher_id='%d', name='%s'", resp.Obj.Publisher.ID, resp.Obj.Publisher.Name)

	return resp.Obj.Publisher.ID, resp.Obj.Publisher.BasePaymentRuleID
}

func apnSetSite(site dbSite) int64 {
	newSite := appnexus.Site{State: site.State, PublisherID: site.PublisherAppnexusID.Int64, Name: site.Name, SupplyType: site.SupplyType}

	if site.URL.Valid {
		newSite.URL = site.URL.String
	}

	if site.SiteAppnexusID.Valid {
		newSite.ID = site.SiteAppnexusID.Int64
		resp, err := apnc.Sites.Update(newSite)
		if err != nil {
			loggerSugar.Errorw(err.Error(), "Site", newSite)
			os.Exit(3)
		}
		loggerSugar.Debugf("Update appnexus_id='%d', name='%s'", resp.Obj.Site.ID, resp.Obj.Site.Name)
		return resp.Obj.Site.ID
	}

	resp, err := apnc.Sites.Add(&newSite)
	if err != nil {
		loggerSugar.Errorw(err.Error(), "Site", newSite)
		os.Exit(3)
	}
	loggerSugar.Debugf("New appnexus_id='%d', name='%s'", resp.Obj.Site.ID, resp.Obj.Site.Name)

	return resp.Obj.Site.ID
}

func apnSetPlacement(placement dbPlacement) int64 {
	newPlacement := appnexus.Placement{PublisherID: placement.PublisherAppnexusID.Int64, Code: placement.Code, State: placement.State, Name: placement.Name}

	if placement.SiteAppnexusID.Valid {
		newPlacement.SiteID = placement.SiteAppnexusID.Int64
	}

	if placement.PlacementAppnexusID.Valid {
		newPlacement.ID = placement.PlacementAppnexusID.Int64
		resp, err := apnc.Placements.Update(newPlacement)
		if err != nil {
			loggerSugar.Errorw(err.Error(), "Placement", newPlacement)
			os.Exit(3)
		}
		loggerSugar.Debugf("Update appnexus_id='%d', name='%s'", resp.Obj.Placement.ID, resp.Obj.Placement.Name)
		return resp.Obj.Placement.ID
	}

	resp, err := apnc.Placements.Add(&newPlacement)
	if err != nil {
		loggerSugar.Errorw(err.Error(), "Placement", newPlacement)
		os.Exit(3)
	}
	loggerSugar.Debugf("New appnexus_id='%d', name='%s'", resp.Obj.Placement.ID, resp.Obj.Placement.Name)

	return resp.Obj.Placement.ID
}

func apnSetDeal(deal dbDeal) int64 {
	newDeal := appnexus.Deal{Code: deal.Code, Name: deal.Name, Active: deal.Active, StartDate: deal.StartDate, EndDate: deal.EndDate}

	if deal.FloorPrice.Valid {
		newDeal.FloorPrice = deal.FloorPrice.Float64
	}

	if deal.DealType == 1 {
		newDeal.AuctionType = &appnexus.AuctionType{ID: 3}
	} else {
		newDeal.AuctionType = &appnexus.AuctionType{ID: 2}
	}

	if deal.DealAppnexusID.Valid {
		newDeal.ID = deal.DealAppnexusID.Int64
		newDeal.Buyer = nil // The buyer cannot be changed
		newDeal.Type = nil  // The type cannot be changed
		resp, err := apnc.Deals.Update(newDeal)
		if err != nil {
			loggerSugar.Errorw(err.Error(), "Deal can`t update", newDeal)
			// loggerSugar.Errorw(err.Error(), "Deal update", newDeal)
			// os.Exit(3)
			return -1 // hack, i like it...
		}

		loggerSugar.Debugf("Update appnexus_id='%d', name='%s'", resp.Obj.Deal.ID, resp.Obj.Deal.Name)
		return resp.Obj.Deal.ID
	}

	newDeal.Type = &appnexus.Type{ID: 2}
	newDeal.Buyer = &appnexus.Buyer{ID: deal.BuyerID}

	resp, err := apnc.Deals.Add(&newDeal)
	if err != nil {
		loggerSugar.Errorw(err.Error(), "Deal can`t insert", newDeal)
		// os.Exit(3)
		return -1 // hack, i like it...
	}
	loggerSugar.Debugf("New appnexus_id='%d', name='%s'", resp.Obj.Deal.ID, resp.Obj.Deal.Name)

	return resp.Obj.Deal.ID
}
