package main

import (
	"go.uber.org/zap"
)

var loggerSugar *zap.SugaredLogger
var loggerCnf zap.Config

func loggerInit() {
	loggerCnf = zap.Config{
		Level:             zap.NewAtomicLevelAt(zap.DebugLevel),
		Development:       false,
		DisableStacktrace: true,
		Encoding:          "console",
		EncoderConfig:     zap.NewDevelopmentEncoderConfig(),
		OutputPaths:       []string{"stdout"},
		ErrorOutputPaths:  []string{"stderr"},
	}
	// log.SetFormatter(&log.JSONFormatter{})
	logger, _ := loggerCnf.Build()
	loggerSugar = logger.Sugar()
}

func loggerSetLevel() {
	if !globalConfig.IsDebug {
		loggerCnf.Level.SetLevel(zap.InfoLevel)
	}
}
