package main

import (
	"encoding/json"
	"os"
)

type config struct {
	Database struct {
		Host     string `json:"host"`
		User     string `json:"user"`
		Password string `json:"password"`
		DBName   string `json:"dbname"`
	} `json:"database"`
	Appnexus struct {
		URI      string `json:"uri"`
		User     string `json:"user"`
		Password string `json:"password"`
	} `json:"appnexus"`
	IsProd  bool `json:"is_prod"`
	IsDebug bool `json:"debug"`
}

var globalConfig config

func loadConfiguration(file string) {
	loggerSugar.Infof("Load config file '%s'", file)

	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		loggerSugar.Errorf("File open error: %s", err.Error())
		os.Exit(1)
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&globalConfig)
	if err != nil {
		loggerSugar.Errorf("JSON decode error: %s", err.Error())
		os.Exit(1)
	}

	errorConfig := ""

	if len(globalConfig.Appnexus.URI) < 5 || len(globalConfig.Appnexus.User) == 0 || len(globalConfig.Appnexus.Password) == 0 {
		errorConfig = "check Appnexus section"
	}

	if len(globalConfig.Database.Host) < 5 || len(globalConfig.Database.DBName) == 0 || len(globalConfig.Database.User) == 0 || len(globalConfig.Database.Password) == 0 {
		errorConfig = "check Database section"
	}

	if errorConfig != "" {
		loggerSugar.Errorf("Bad JSON config, %s", errorConfig)
		os.Exit(1)
	}

}
