package main

import (
	"os"
	"strings"

	"gopkg.in/alecthomas/kingpin.v2"
)

const (
	constAll       = "all"
	constInventory = "inventory"
	constDeal      = "deal"
	version        = "1.6"
)

var (
	app         = kingpin.New("karan", "A command-line application for AppNexus Inventory sync.").Version(version)
	configPath  = app.Arg("path", "Config file path in JSON format").Required().String()
	configSyncs = app.Flag("sync", "Choose what to sync: inventory, deal, all (default)").String()
)

func init() {
	loggerInit()
}

func main() {
	app.Parse(os.Args[1:])

	if *configPath == "" {
		app.FatalUsage("Please add valid path for config file")
		return
	}

	if *configSyncs == "" {
		*configSyncs = constAll
	} else {
		*configSyncs = strings.ToLower(*configSyncs)
		if *configSyncs != constAll && *configSyncs != constInventory && *configSyncs != constDeal {
			app.FatalUsage("Unknown sync argument '%s'", *configSyncs)
			return
		}
	}

	loggerSugar.Infof("KARAN v%s started", version)

	loadConfiguration(*configPath)
	loggerSetLevel()
	loggerSugar.Debugf("Sync %s", *configSyncs)

	apnInit()
	dbInit()

	// inventory
	if *configSyncs == constAll || *configSyncs == constInventory {
		syncPublishers()
		syncSites()
		syncPlacements()
	}

	// deals
	if *configSyncs == constAll || *configSyncs == constDeal {
		syncDeals()
	}

	loggerSugar.Infof("Done, exit")
	loggerSugar.Sync()
}

func syncPublishers() {
	loggerSugar.Infof("Publishers sync begin")

	kargoPubs := dbGetPublishers()
	loggerSugar.Debugf("Found %d unsynced pubs", len(kargoPubs))
	for _, element := range kargoPubs {
		loggerSugar.Debugf("PublisherID='%d' start sync", element.PublisherID)
		apnID, apnRuleID := apnSetPublisher(element)
		dbSetPublisher(apnID, apnRuleID, element.PublisherID)
	}
}

func syncSites() {
	loggerSugar.Infof("Sites sync begin")

	kargoSites := dbGetSites()
	loggerSugar.Debugf("Found %d unsynced sites", len(kargoSites))
	for _, element := range kargoSites {
		loggerSugar.Debugf("PublisherID='%d' SiteID='%d' start sync", element.PublisherID, element.SiteID)
		apnSiteID := apnSetSite(element)
		dbSetSite(apnSiteID, element.SiteID)
	}
}

func syncPlacements() {
	loggerSugar.Infof("Placements sync begin")

	kargoPlacements := dbGetPlacements()
	loggerSugar.Debugf("Found %d unsynced placements", len(kargoPlacements))
	for _, element := range kargoPlacements {
		loggerSugar.Debugf("PublisherID='%d' SiteID='%d' PlacementID='%d' start sync", element.PublisherID, element.SiteID, element.PlacementID)
		apnPlacementID := apnSetPlacement(element)
		dbSetPlacement(apnPlacementID, element.PlacementID)
	}
}

func syncDeals() {
	loggerSugar.Infof("Deals+Seat sync begin")

	dbUpdateDeals()

	kargoDeals := dbGetDeals()
	loggerSugar.Debugf("Found %d unsynced deals+seats", len(kargoDeals))
	for _, element := range kargoDeals {
		loggerSugar.Debugf("DealID='%d' SeatID='%d' Code='%s' start sync", element.DealID, element.SeatID, element.Code)
		apnDealID := apnSetDeal(element)
		if apnDealID != -1 {
			dbSetDeal(apnDealID, element.DealID, element.SeatID)
		}
	}
}
