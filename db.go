package main

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB
var stmts map[string]*sql.Stmt

func dbInit() {
	loggerSugar.Debugf("Connect to MySQL DB on '%s'", globalConfig.Database.Host)
	var err error
	db, err = sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", globalConfig.Database.User, globalConfig.Database.Password, globalConfig.Database.Host, globalConfig.Database.DBName))
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}

	stmts = make(map[string]*sql.Stmt)
	stmts["upub"], err = db.Prepare("UPDATE publisher SET appnexus_modified_at = NOW(), appnexus_id = ?, appnexus_base_payment_rule_id = ? WHERE id = ?")
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}
	stmts["usite"], err = db.Prepare("UPDATE site SET appnexus_modified_at = NOW(), appnexus_id = ? WHERE id = ?")
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}
	stmts["uplcmt"], err = db.Prepare("UPDATE placement SET appnexus_modified_at = NOW(), appnexus_id = ? WHERE id = ?")
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}
	stmts["udeal"], err = db.Prepare("UPDATE appnexus_deal_seat SET appnexus_modified_at = NOW(), appnexus_id = ? WHERE deal_id = ? AND seat_id = ?")
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}
}

func checkErr(err error) {
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}
}

type dbPublisher struct {
	PublisherID         int64
	PublisherAppnexusID sql.NullInt64
	State               string
	Name                string
}

type dbSite struct {
	SiteID              int64
	PublisherID         int64
	SiteAppnexusID      sql.NullInt64
	PublisherAppnexusID sql.NullInt64
	URL                 sql.NullString
	State               string
	SupplyType          string
	Name                string
}

type dbPlacement struct {
	SiteID              int64
	PublisherID         int64
	PlacementID         int64
	SiteAppnexusID      sql.NullInt64
	PublisherAppnexusID sql.NullInt64
	PlacementAppnexusID sql.NullInt64
	Name                string
	Code                string
	State               string
}

type dbDeal struct {
	DealID         int64
	SeatID         int64
	BuyerID        int64
	DealType       int
	FloorPrice     sql.NullFloat64
	Code           string
	Name           string
	StartDate      string
	EndDate        string
	Active         bool
	DealAppnexusID sql.NullInt64
}

func dbGetPublishers() []dbPublisher {
	rows, err := db.Query(`SELECT
    A.id AS publisher_id,
    P.appnexus_id AS publisher_appnexus_id,
    A.name AS name,
    IF(P.publisher_status_id = 1 AND A.status_id = 1, 'active', 'inactive') AS state
FROM
    account A
    INNER JOIN publisher P ON P.id = A.id
    LEFT JOIN info I ON A.info_id = I.id
WHERE
    A.account_type_id = 1
    AND (
        (P.publisher_status_id = 1 AND A.status_id = 1 AND P.appnexus_modified_at IS NULL)
        OR (P.appnexus_modified_at < IFNULL(I.modified_at, I.created_at))
    );`)

	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}

	items := []dbPublisher{}

	for rows.Next() {
		var row dbPublisher
		err = rows.Scan(&row.PublisherID, &row.PublisherAppnexusID, &row.Name, &row.State)
		if err != nil {
			loggerSugar.Errorw(err.Error())
			os.Exit(2)
		}
		items = append(items, row)
	}

	return items
}

func dbSetPublisher(apnID int64, apnRuleID int64, pubID int64) {
	_, err := stmts["upub"].Exec(apnID, apnRuleID, pubID)
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}
	loggerSugar.Debugf("Kargo publisher='%d' update with appnexus_id='%d' appnexus_base_payment_rule_id='%d'", pubID, apnID, apnRuleID)
}

func dbGetSites() []dbSite {
	rows, err := db.Query(`SELECT
    A.id AS publisher_id,
		S.id AS site_id,
    S.name AS name,
    P.appnexus_id AS publisher_appnexus_id,
    S.appnexus_id AS site_appnexus_id,
    S.url,
    IF(S.application = 1, 'mobile_app', 'web') AS supply_type,
    IF(P.publisher_status_id = 1 AND A.status_id = 1 AND S.status_id = 1, 'active', 'inactive') AS state
FROM
    site S
    INNER JOIN account A ON A.id = S.publisher_account_id
    INNER JOIN publisher P ON P.id = A.id
    LEFT JOIN info I ON S.info_id = I.id
    LEFT JOIN info I2 ON A.info_id = I2.id
WHERE
    (
        (P.publisher_status_id = 1 AND A.status_id = 1 AND S.status_id = 1 AND S.appnexus_modified_at IS NULL)
        OR (S.appnexus_modified_at < IFNULL(I.modified_at, I.created_at))
        OR (P.appnexus_modified_at < IFNULL(I2.modified_at, I2.created_at))
    )`)

	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}

	items := []dbSite{}

	for rows.Next() {
		var row dbSite
		err = rows.Scan(&row.PublisherID, &row.SiteID, &row.Name, &row.PublisherAppnexusID, &row.SiteAppnexusID, &row.URL, &row.SupplyType, &row.State)
		if err != nil {
			loggerSugar.Errorw(err.Error())
			os.Exit(2)
		}
		if row.PublisherAppnexusID.Valid {
			items = append(items, row)
		} else {
			loggerSugar.Warnf("Ignore site without publisher sync: %#v", row)
		}
	}

	return items
}

func dbSetSite(apnSiteID int64, siteID int64) {
	_, err := stmts["usite"].Exec(apnSiteID, siteID)
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}
	loggerSugar.Debugf("Kargo site='%d' update with appnexus_id='%d'", siteID, apnSiteID)
}

func dbGetPlacements() []dbPlacement {
	rows, err := db.Query(`SELECT
    A.id AS publisher_id,
    S.id AS site_id,
    PL.id AS placement_id,
    P.appnexus_id AS publisher_appnexus_id,
    S.appnexus_id AS site_appnexus_id,
    PL.appnexus_id AS placement_appnexus_id,
    CONCAT(S.name, '_', PL.name, '_', PL.id) AS name,
    PL.id AS code,
    IF(P.publisher_status_id = 1 AND A.status_id = 1 AND S.status_id = 1 AND PL.status_id = 1, 'active', 'inactive') AS state
FROM
    placement PL
    INNER JOIN site S ON S.id = PL.site_id
    INNER JOIN account A ON A.id = S.publisher_account_id
    INNER JOIN publisher P ON P.id = A.id
    LEFT JOIN info I ON PL.info_id = I.id
    LEFT JOIN info I2 ON S.info_id = I2.id
    LEFT JOIN info I3 ON A.info_id = I3.id
WHERE
    S.appnexus_id IS NOT NULL AND (
        (P.publisher_status_id = 1 AND A.status_id = 1 AND S.status_id = 1 AND PL.status_id = 1 AND PL.appnexus_modified_at IS NULL)
        OR (PL.appnexus_modified_at < IFNULL(I.modified_at, I.created_at))
        OR (S.appnexus_modified_at < IFNULL(I2.modified_at, I2.created_at))
        OR (P.appnexus_modified_at < IFNULL(I3.modified_at, I3.created_at))
    );`)

	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}

	items := []dbPlacement{}

	for rows.Next() {
		var row dbPlacement
		err = rows.Scan(&row.PublisherID, &row.SiteID, &row.PlacementID, &row.PublisherAppnexusID, &row.SiteAppnexusID, &row.PlacementAppnexusID, &row.Name, &row.Code, &row.State)
		if err != nil {
			loggerSugar.Errorw(err.Error())
			os.Exit(2)
		}
		if row.PublisherAppnexusID.Valid {
			if row.SiteAppnexusID.Valid {
				items = append(items, row)
			} else {
				loggerSugar.Warnf("Ignore placement without site sync: %#v", row)
			}
		} else {
			loggerSugar.Warnf("Ignore placement without publisher sync: %#v", row)
		}
	}

	return items
}

func dbSetPlacement(apnPlacementID int64, placementID int64) {
	_, err := stmts["uplcmt"].Exec(apnPlacementID, placementID)
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}
	loggerSugar.Debugf("Kargo placement='%d' update with appnexus_id='%d'", placementID, apnPlacementID)
}

func dbUpdateDeals() {
	res, err := db.Exec(`INSERT IGNORE INTO appnexus_deal_seat
SELECT
    DS.deal_id AS deal_id,
    DS.seat_id AS seat_id,
    NOW() AS modified_at,
    NULL AS appnexus_id,
    NULL AS appnexus_modified_at,
    'active' AS appnexus_state
FROM deal_seat DS;`)

	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}

	rows, err := res.RowsAffected()
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}

	insertedRows := rows

	res, err = db.Exec(`UPDATE
    appnexus_deal_seat ADS
    LEFT JOIN deal_seat DS ON DS.deal_id = ADS.deal_id AND DS.seat_id = ADS.seat_id
SET
    ADS.modified_at = NOW(),
    ADS.appnexus_state = 'inactive'
WHERE
    DS.deal_id IS NULL AND DS.seat_id IS NULL;`)

	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}

	rows, err = res.RowsAffected()
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}

	loggerSugar.Debugf("Kargo deals+seats prepare: inserted='%d', updated='%d'", insertedRows, rows)
}

func dbGetDeals() []dbDeal {
	var bidderID int
	if globalConfig.IsProd {
		bidderID = 29
	} else {
		bidderID = 71
	}
	queryStr := fmt.Sprintf(`SELECT
	    DS.deal_id AS deal_id,
	    DS.seat_id AS seat_id,
	    D.deal_id AS code,
	    D.name AS name,
	    D.start_date AS start_date,
	    D.end_date AS end_date,
	    IF(D.deal_type_id = 1, D.first_look_cpm, D.bid_floor) AS floor_price,
			D.deal_type_id,
	    S.seat_id AS buyer,
	    IF(D.ad_server_status_id = 1 AND D.status_id = 1 AND S.status_id = 1 AND B.status_id = 1, 'true', 'false') AS active,
			DS.appnexus_id AS deal_appnexus_id
	FROM
	    deal D
	    INNER JOIN appnexus_deal_seat DS ON DS.deal_id = D.id
	    INNER JOIN seat S ON DS.seat_id = S.id
	    INNER JOIN bidder B ON B.id = S.bidder_id
	    LEFT JOIN info I ON D.info_id = I.id
	    LEFT JOIN info I2 ON S.info_id = I2.id
	    LEFT JOIN info I3 ON B.info_id = I3.id
	WHERE
	    B.id = %d
	    AND (
	        (D.ad_server_status_id = 1 AND D.status_id = 1 AND S.status_id = 1 AND B.status_id = 1 AND DS.appnexus_modified_at IS NULL)
	        OR (DS.appnexus_modified_at < IFNULL(I.modified_at, I.created_at))
	        OR (DS.appnexus_modified_at < IFNULL(I2.modified_at, I2.created_at))
	        OR (DS.appnexus_modified_at < IFNULL(I3.modified_at, I3.created_at))
	    )`, bidderID)
	rows, err := db.Query(queryStr)

	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}

	items := []dbDeal{}

	for rows.Next() {
		var row dbDeal
		err = rows.Scan(&row.DealID, &row.SeatID, &row.Code, &row.Name, &row.StartDate, &row.EndDate, &row.FloorPrice, &row.DealType, &row.BuyerID, &row.Active, &row.DealAppnexusID)
		if err != nil {
			loggerSugar.Errorw(err.Error())
			os.Exit(2)
		}
		items = append(items, row)
	}

	return items
}

func dbSetDeal(apnDealID int64, dealID int64, seatID int64) {
	_, err := stmts["udeal"].Exec(apnDealID, dealID, seatID)
	if err != nil {
		loggerSugar.Errorw(err.Error())
		os.Exit(2)
	}
	loggerSugar.Debugf("Kargo DealID='%d' SeatID='%d' update with appnexus_id='%d'", dealID, seatID, apnDealID)
}
